package problema;

import java.util.Scanner;

public class PrimeTest {

	public static boolean isPrime(int number) {
		for (int i = 2; i < number / 2; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static void check(int n1, int n2) {
		if(n1 > n2) {
			System.out.println("The second input number should be greater or equal than the first one!");
			return;
		}
		
		boolean isFound = false;
		for (int i = n1; i <= n2; i++) {
			if (isPrime(i) && 
					(i<-1 || i > 1)) { //excluding the case where i is -1, 0 or 1
				isFound = true;
				break;
			}
		}

		if (isFound) {
			System.out.println("There is at least one prime number in the range [" + n1 + ";" + n2 + "].");
		} else {
			System.out.println("There is no prime numbers in the range [" + n1 + ";" + n2 + "].");
		}
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the first number:");
		int n1 = 0;
		try {
			n1 = scan.nextInt();	
		} catch(Exception e) {
			System.out.println("This input number should be an integer!");
			e.printStackTrace();
			System.exit(0);
		}
		
		System.out.println("Enter the second number:");
		int n2 = 0;
		try {
			n2 = scan.nextInt();	
		} catch(Exception e) {
			System.out.println("This input number should be an integer!");
			e.printStackTrace();
			System.exit(0);
		}
		
		scan.close();
		check(n1, n2);
	}
}
